(ns graphclj.tools
  (:require [clojure.string :as str]))

(defn readfile [f]
    "Returns a sequence from a file f"
    (with-open [rdr (clojure.java.io/reader f)]
            (doall (line-seq rdr))))

(defn vals-label [g l]
  (loop [k g, res []]
    (if (seq k)
      (recur (rest k) (into [] (conj res (get (second (first k)) l))))
      res)))

(defn sort-rank [srt]
  (let [tot (count srt)]
    (loop [t 0, s srt, res {}]
      (if (seq s)
        (if (in? (keys res) (first s))
          (recur t (rest s) res)
          (if (empty? (filter (fn [x] (< x (first s))) (into [] (keys res))))
            (recur t (rest s) (conj res (vector (first s) t)))
            (recur (+ t 1) (rest s) (conj res (vector (first s) (+ t 1))))))
        res))))

(defn rank-nodes [g l]
  "Ranks the nodes of the graph in relation to label l in accending order"
  (let [fut (vals-label g l)
        srt (into [] (sort fut))]
    (let [sorted (sort-rank srt)]
      (println sorted)
      (loop [s g, res {}]
        (if (seq s)
          (recur (rest s) (conj res (vector (key (first s)) (conj (val (first s)) (vector :rank (get sorted (get (val (first s)) l)))))))
          res)))))

(vals-label (closeness-all test) :close)
(rank-nodes (closeness-all test) :close)


(defn generate-colors [n]
    (let [step 10]
     (loop [colors {}, current [255.0 160.0 122.0], c 0]
       (if (= c (inc n))
         colors
         (recur (assoc colors c (map #(/ (mod (+ step %) 255) 255) current))
                (map #(mod (+ step %) 255) current) (inc c))))))




(defn to-dot [g]
  "Returns a string in dot format for graph g, each node is colored in relation to its ranking")