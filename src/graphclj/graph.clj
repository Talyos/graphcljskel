(ns graphclj.graph
  (:require [clojure.string :as str]))

;; Generate a graph from the lines
(defn gen-graph [lines]
  (loop [s lines, res {}]
    (if (seq s)
      (let [t (parse-int (first s))]
        (recur (rest s) (conj (conj res (vector (second t) (array-map :neigh (conj (get (get res (second t)) :neigh #{}) (first t))))) (vector (first t) (array-map :neigh (conj (get (get res (first t)) :neigh #{}) (second t)))))))
      res)))
"Returns a hashmap contating the graph"

(defn parse-int [s]
  (map read-string (re-seq #"[\d.]+" s)))

(defn erdos-renyi-rnd [n,p]
  (loop [k n, res {}]
    (if (> k 0)
      (recur (- k 1) (conj res (vector (- k 1) (array-map :neigh (into #{} (filter (fn [x] (if (= x (- k 1)) false true)) (range n)))))))
      res)))
"Returns a G_{n,p} random graph, also known as an Erdős-Rényi graph"

(defn erdos-proba [] nil)