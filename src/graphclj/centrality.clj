(ns graphclj.centrality
    (:require [graphclj.graph :as graph]
              [clojure.set :as set]))

(let [g {1 {:neigh #{0 5 4 3}}, 2 {:neigh #{1 0}}}]
  (conj g (vector 2 (conj (get g 2) (array-map :degree (count (get (get g 2) :neigh)))))))

(defn degrees [g]
  "Calculates the degree centrality for each node"
  (loop [s g, res {}]
    (if (seq s)
      (let [k (key (first s))]
        (recur (rest s) (conj res (vector k (conj (get g k) (array-map :degree (count (get (get g k) :neigh))))))))
      res)))

(degrees {1 {:neigh #{0 5 4 3}}, 2 {:neigh #{3 4}}})


(defn add-all [i n]
  (loop [res {}, g n]
    (if (seq g)
      (recur (conj res (vector (first g) 1.0)) (rest g))
      res)))

(defn init-all [init manquants]
  (loop [s manquants, res init]
    (if (seq s)
      (recur (rest s) (conj res (vector (first s) 10.0)))
      res)))

(defn common-elements [& colls]
  (let [freqs (map frequencies colls)]
    (mapcat (fn [e] (repeat (apply min (map #(% e) freqs)) e))
      (apply clojure.set/intersection (map (comp set keys) freqs)))))


(defn in?
  "true if coll contains elm"
  [coll elm]
  (some #(= elm %) coll))

(defn distance [g n]
  "Calculate the distances of one node to all the others"
      (let [init (add-all n (get (get g n) :neigh))]
        (let [manquants (into #{} (filter (fn [x] (if (= x n) false (not (in? (keys init) x)))) (range (count (keys g)))))]
          (loop [conn (get (get g n) :neigh)
                 res (init-all init manquants)
                 mqt manquants]
            (if (seq mqt)
              (let [presents (common-elements (get (get g (first conn)) :neigh) manquants)]
                (if (empty? presents)
                  (recur (disj conn (first conn)) res mqt)
                  (let [dist (+ (get res (first conn)) 1)]
                    (if (< dist (get res (first mqt)))
                       (recur conn (conj res (vector (first mqt) dist)) (disj mqt (first mqt)))
                       (recur conn res mqt)))))
              (conj res (vector n 0.0)))))))

(def test {0 {:neigh #{1 3}}, 1 {:neigh #{0 3 4}}, 2 {:neigh #{3}}, 3 {:neigh #{1 2 0}}, 4 {:neigh #{1}}})
(distance test 1)

(defn closeness [g n]
  "Returns the closeness for node n in graph g"
  (let [dists (distance g n)]
    (let [inv (map (fn [x] (/ 1 x)) (into [] (filter #(not= % 0.0) (vals dists))))]
      (reduce + inv))))


(closeness test 1)

(closeness-all test)

(defn closeness-all [g]
  "Returns the closeness for all nodes in graph g"
  (loop [k (keys g), res g]
    (if (seq k)
      (let [cls (closeness g (first k))]
        (recur (rest k) (conj res (vector (first k) (conj (get g (first k)) (vector :close cls))))))
      res)))